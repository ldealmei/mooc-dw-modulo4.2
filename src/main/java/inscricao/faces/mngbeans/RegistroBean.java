/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.faces.mngbeans;

import inscricao.entity.Candidato;
import java.util.ArrayList;
import javax.inject.Named;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author Lucas
 */
@Named(value = "registroBean")
@ManagedBean
@ApplicationScoped
public class RegistroBean extends utfpr.faces.support.PageBean {

    private ArrayList<Candidato> CandidatosList;

    public ArrayList<Candidato> getCandidatosList() {
        return CandidatosList;
    }

    public void setCandidatosList(ArrayList<Candidato> CandidatosList) {
        this.CandidatosList = CandidatosList;
    }
    /**
     * Creates a new instance of RegistroBean
     */
    public RegistroBean() {
        CandidatosList = new ArrayList<Candidato>();
    }

    public void addCandidato(Candidato candidato) {
        CandidatosList.add(candidato);
    }
    
    
}
